<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
<!ENTITY % BOOK_ENTITIES SYSTEM "Web_Server.ent">
%BOOK_ENTITIES;
]>
<article>
  <xi:include href="Article_Info.xml" xmlns:xi="http://www.w3.org/2001/XInclude" />
  <section>
    <title>Introduction</title>
    <para>
      During a search operation, net control must not only log radio
      traffic but also keep track of all the teams in the field.  The
      safety of the searchers is dependent on the ability of net
      control to maintain status on each of the teams.  As a result,
      thelogging needs of net control are somewhat different than
      ordinary amateur practice.
    </para>
    <para>
      To facilitate this logging, a custom logging web application has
      been made available.  This application is intended to run on a
      small, self contained web server which resides on the search
      private wireless network.  No Internet connection is required.
    </para>
  </section>
  <section>
    <title>The Web Server</title>
    <para>
      The web server consists of a small black box, approximately
      10x5x4cm.  At one end of the box is a wire terminating in a
      standard USB plug.  On the other end of the box isan RJ45
      Ethernet cable.  There are a number of ventilation holes punched
      in the side of the box. In addition, there is a cell phone
      charger which accepts the USB plug.
    </para>
    <para>
      Inside the black box is a Raspberry Pi Model B computer.  The
      computer is running Fedora 18 Spherical Cow, Apache 2.4, MySQL
      5.5 and PHP 5.4.  The firewalld daemon opens the http port, port
      80.  In addition, the ssh port is open.  Other ports are closed.
    </para>
  </section>
  <section>
    <title>Setting up the web server</title>
    <para>
      To prepare the server for a search, connect the power and
      connect a network cable between the server and a wireless
      router.  It will take approximately three minutes for the server
      to boot, start the MySQL server, and web server.
    </para>
    <section>
      <title>Setting the date</title>
      <para>
	If there is no Internet connection available, it is necessary
	to manually set the date on the server before beginning
	logging.
      </para>
      <para>
	Log on to the web server using a secure shell client such as
	the default ssh client:
<screen>
$ ssh saruser@192.168.0.62
saruser@192.168.0.62's password: 

</screen>
        or using a GUI client such as <application>PuTTY</application>.
      </para>
      <para>
	When logging on use the username <filename>saruser</filename>
	and the password <filename>sarpassword</filename>.  The
	server's IP address is fixed at
	<filename>192.168.0.62</filename>.  The server is accessed by
	IP address since there is rarely a name server available.
      </para>
      <para>
	On logging on, you will see a welcome banner and a prompt:
<screen>
[jjmcd@Aidan Web_Server]$ ssh saruser@192.168.0.62
saruser@192.168.0.62's password: 
Last login: Fri Feb 15 14:47:01 2013 from 192.68.0.18

     _                                               _
     _              S A R s e r v e r                _
     _                                               _
                                         Spherical Cow
[saruser@sarserver ~]$ 

</screen>
      </para>
      <para>
	The date is entered using the <command>date</command> command,
	however, it must be preceded by the command
	<command>sudo</command> since this command affects the entire
	system rather than the individual user.  The date itself must
	be in the form "YYYY-MM-DD hh:mm:ss".  The date is preceded by
	the <command>-s</command> switch (set the time and date) and
	the date itself, since it contains a space, must be surrounded
	by quotes.
      </para>
      <para>
	The system will echo the date in a different format:
<screen>
[saruser@sarserver ~]$ sudo date -s '2013-02-15 14:54:30'
Fri Feb 15 14:54:30 EST 2013
[saruser@sarserver ~]$ 

</screen>
      </para>
      <para>
	Note that if the Internet is available, the system will set
	the time and date automatically.
      </para>
      <para>
	After setting the date, log off by typing
	<command>exit</command> or <command>logout</command> or
	<command>Ctrl+D</command>.
      </para>
      <para>
<screen>
[saruser@sarserver ~]$ exit
logout
Connection to 192.168.0.62 closed.
[jjmcd@Aidan Web_Server]$

</screen>
      </para>
    </section>
    <section>
      <title>Shutting down the server</title>
      <para>
	After the operation is complete, the server should be shut
	down cleanly to reduce the possibility of disk corruption.
      </para>
      <para>
	Log on to the server as before, and type the shutdown command
	<command>sudo shutdown -h now</command>.  The ssh connection
	will immediately be terminated, but the shutdown will take a
	little time.
<screen>
[saruser@sarserver ~]$ sudo shutdown -h now
Connection to 192.168.0.62 closed by remote host.
Connection to 192.168.0.62 closed.
[jjmcd@Aidan Web_Server]$ 

</screen>
      </para>
      <para>
	Wait about two minutes before removing
	power. (The network connection may be removed at any time.
      </para>
    </section>
  </section>
  <section>
    <title>Connecting to the server</title>
    <para>
      <figure float="0">
	<title>SAR menu</title>
	<mediaobject>
	  <imageobject>
	    <imagedata scale="99" scalefit="1"
                         fileref="images/SARmenu01.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <para>
	      Search Menu
	    </para>
	  </textobject>
          </mediaobject>
      </figure>
      </para>
  </section>

  <xi:include href="Revision_History.xml" xmlns:xi="http://www.w3.org/2001/XInclude" />
  <index />
</article>

